package com.annimon.voicyanski.exceptions;

import com.annimon.voicyanski.Main;

public class AudioConvertException extends RuntimeException {

    public AudioConvertException() {
        super("Unable to convert audio file");
    }

    @Override
    public String getLocalizedMessage() {
        return Main.getResources().getString("unable_convert_audio");
    }
}
