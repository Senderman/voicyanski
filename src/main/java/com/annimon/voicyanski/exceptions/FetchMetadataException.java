package com.annimon.voicyanski.exceptions;

import com.annimon.voicyanski.Main;

public class FetchMetadataException extends RuntimeException {

    public FetchMetadataException() {
        super("Unable to fetch metadata");
    }

    @Override
    public String getLocalizedMessage() {
        return Main.getResources().getString("unable_fetch_metadata");
    }
}
