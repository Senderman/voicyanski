package com.annimon.voicyanski.exceptions;

import com.annimon.voicyanski.Main;

public class WaveformBuildException extends RuntimeException {

    public WaveformBuildException(Throwable cause) {
        super("Unable to build waveform", cause);
    }

    @Override
    public String getLocalizedMessage() {
        return Main.getResources().getString("unable_build_waveform");
    }
}
