package com.annimon.voicyanski.tasks;

import com.annimon.voicyanski.audio.Waveform;
import com.annimon.voicyanski.audio.WaveformFileReader;
import com.annimon.voicyanski.exceptions.WaveformBuildException;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public final class BuildWaveformTask implements Function<Metadata, Waveform> {

    private static final int PROCESS_WAIT_TIME = 30;

    @Override
    public Waveform apply(Metadata metadata) {
        File tmpLogFile = new File("log" + System.currentTimeMillis() +".txt");
        try {
            final ProcessBuilder pb = new ProcessBuilder(
                    "ffmpeg",
                    "-v", "error",
                    "-i", metadata.file.getAbsolutePath(),
                    "-af", "astats=metadata=1:reset=1,ametadata=print"
                            + ":key=lavfi.astats.Overall.RMS_level"
                            + ":file=" + tmpLogFile.getName(),
                    "-f", "null", "-"
            );
            final Process ffmpeg = pb.start();
            boolean status = ffmpeg.waitFor(PROCESS_WAIT_TIME, TimeUnit.SECONDS);
            status &= ffmpeg.exitValue() == 0;
            if (status) {
                return new WaveformFileReader(tmpLogFile.toPath()).read();
            }
        } catch (InterruptedException | IOException ex) {
            throw new WaveformBuildException(ex);
        } finally {
            if (tmpLogFile.exists()) {
                tmpLogFile.delete();
            }
        }
        return null;
    }
}
