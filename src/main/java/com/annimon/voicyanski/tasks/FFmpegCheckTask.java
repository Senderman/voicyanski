package com.annimon.voicyanski.tasks;

import com.annimon.voicyanski.exceptions.FFmpegNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public final class FFmpegCheckTask implements Supplier<Boolean> {

    private static final int PROCESS_WAIT_TIME = 5;

    @Override
    public Boolean get() {
        try {
            Process ffmpeg = Runtime.getRuntime().exec("ffmpeg -version");
            return ffmpeg.waitFor(PROCESS_WAIT_TIME, TimeUnit.SECONDS);
        } catch (InterruptedException | IOException ex) {
            throw new FFmpegNotFoundException();
        }
    }
}
