package com.annimon.voicyanski.tasks;

import com.annimon.voicyanski.Main;
import com.annimon.voicyanski.exceptions.AudioConvertException;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.UnaryOperator;

public final class OpusConvertTask implements UnaryOperator<ClipPartData> {

    private static final int PROCESS_WAIT_TIME = 30;

    @Override
    public ClipPartData apply(ClipPartData data) {
        data.outputAudioFile = new File("clip" + System.currentTimeMillis() +".ogg");
        data.outputAudioFile.deleteOnExit();
        try {
            final ProcessBuilder pb = new ProcessBuilder(
                    "ffmpeg",
                    "-v", "error",
                    "-ss", Double.toString(data.startTime),
                    "-i", data.metadata.file.getAbsolutePath(),
                    "-t", Double.toString(data.endTime - data.startTime),
                    "-ac", Main.getConfig().isEncodeStereo() ? "2" : "1",
                    "-map", "0:a",
                    "-codec:a", "libopus",
                    "-b:a", Main.getConfig().getEncodeBitrate(),
                    "-vbr", "off",
                    "-ar", Main.getConfig().getEncodeSampleRate(),
                    data.outputAudioFile.getAbsolutePath()
            );
            final Process ffmpeg = pb.start();
            boolean status = ffmpeg.waitFor(PROCESS_WAIT_TIME, TimeUnit.SECONDS);
            status &= ffmpeg.exitValue() == 0;
            status &= data.outputAudioFile.exists();
            status &= data.outputAudioFile.length() > 0;
            if (status) {
                return data;
            }
        } catch (InterruptedException | IOException ex) {
            throw new AudioConvertException();
        }
        return null;
    }
}
