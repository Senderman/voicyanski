package com.annimon.voicyanski;

import com.annimon.voicyanski.audio.Waveform;
import com.annimon.voicyanski.controls.AudioClipView;
import com.annimon.voicyanski.tasks.BuildWaveformTask;
import com.annimon.voicyanski.tasks.ClipPartData;
import com.annimon.voicyanski.tasks.FFmpegCheckTask;
import com.annimon.voicyanski.tasks.FetchMetadataTask;
import com.annimon.voicyanski.tasks.OpusConvertTask;
import com.annimon.voicyanski.tasks.SendTelegramVoiceTask;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Stream;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import org.kordamp.ikonli.javafx.FontIcon;

public class MainController implements Initializable {

    @FXML
    private Button btnMarkStart, btnMarkEnd, btnBackward, btnPlay, btnForward, btnSend;

    @FXML
    private FontIcon iconPlay;

    @FXML
    private AudioClipView clipView;

    public void setParameters(Application.Parameters parameters) {
        if (hasValidFilenames(parameters.getRaw().stream())) {
            final Stream<File> stream = parameters.getRaw().stream()
                    .map(File::new)
                    .filter(File::isFile)
                    .filter(File::canRead);
            processFiles(stream, () -> {});
        }
    }

    public void setPrimaryStage(Stage stage) {
        stage.titleProperty().bind(Bindings
                .when(clipView.metadataProperty().isNull())
                .then(Main.getResources().getString("voicyanski"))
                .otherwise(Bindings.format("%s [%s]",
                        clipView.metadataProperty().asString(),
                        clipView.durationProperty()
                ))
        );
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        checkFFmpegExists();
        initDragDropEvents();
        initButtonsBinding();
        clipView.initMarkButtons(btnMarkStart, btnMarkEnd);
        clipView.initPlayButtons(btnBackward, btnPlay, btnForward);
        clipView.setIconPlay(iconPlay);
        btnSend.setOnAction(event -> {
            final ClipPartData data = new ClipPartData();
            data.metadata = clipView.getMetadata();
            data.startTime = clipView.getClipPartStart();
            data.endTime = clipView.getClipPartEnd();
            if(data.endTime - data.startTime > 5.0) {
                btnSend.setDisable(true);
                CompletableFuture.completedFuture(data)
                        .thenApplyAsync(new OpusConvertTask())
                        .thenApplyAsync(new SendTelegramVoiceTask())
                        .handleAsync(this::handleVoiceSend);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle(Main.getResources().getString("error"));
                alert.setHeaderText(null);
                alert.setContentText(Main.getResources().getString("zero_file_length"));
                alert.showAndWait();
            }
        });
    }

    private void checkFFmpegExists() {
        CompletableFuture.supplyAsync(new FFmpegCheckTask())
                .handle(this::handleFFmpegCheck);
    }

    private Boolean handleFFmpegCheck(Boolean result, Throwable ex) {
        Platform.runLater(() -> {
            if (ex != null) {
                alertError(ex);
            }
        });
        return result;
    }

    private void initDragDropEvents() {
        clipView.setOnDragOver(event -> {
            final Dragboard db = event.getDragboard();
            boolean status = db.hasFiles();
            status &= hasValidFilenames(db.getFiles().stream().map(File::getName));
            if (status) {
                event.acceptTransferModes(TransferMode.COPY);
            } else {
                event.consume();
            }
        });
        clipView.setOnDragDropped(event -> {
            final Dragboard db = event.getDragboard();
            if (!db.hasFiles()) {
                event.setDropCompleted(false);
                event.consume();
                return;
            }
            
            clipView.reset();
            processFiles(db.getFiles().stream(), () -> event.setDropCompleted(true));
            event.consume();
        });
    }

    private boolean hasValidFilenames(Stream<String> stream) {
        final String[] supportedExts = {".mp3", ".wav", ".aac"};
        return stream
                .map(String::toLowerCase)
                .allMatch(name -> Arrays.stream(supportedExts)
                        .anyMatch(ext -> name.endsWith(ext)));
    }

    private void processFiles(Stream<File> stream, Runnable action) {
        stream.findAny()
                .ifPresent(f -> {
                    CompletableFuture.completedFuture(f)
                            .thenApplyAsync(new FetchMetadataTask())
                            .thenApplyAsync(metadata -> {
                                Platform.runLater(() -> clipView.setMetadata(metadata));
                                return metadata;
                            })
                            .thenApplyAsync(new BuildWaveformTask())
                            .handleAsync(this::handleFile);
                    action.run();
                });
    }

    private void initButtonsBinding() {
        final BooleanBinding waveformExistsProperty = clipView.waveformProperty().isNotNull();
        for (Button btn : Arrays.asList(btnMarkStart, btnMarkEnd,
                btnBackward, btnPlay, btnForward, btnSend)) {
            btn.visibleProperty().bind(waveformExistsProperty);
        }
    }

    private Waveform handleFile(Waveform waveform, Throwable ex) {
        Platform.runLater(() -> {
            if (waveform == null || ex != null) {
                alertError(ex);
                clipView.reset();
            } else {
                clipView.onLoad(waveform);
            }
        });
        return waveform;
    }

    private Boolean handleVoiceSend(Boolean result, Throwable ex) {
        Platform.runLater(() -> {
            btnSend.setDisable(false);
            if (ex != null) {
                alertError(ex);
            } else if (result) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle(Main.getResources().getString("sent"));
                alert.setHeaderText(null);
                alert.setContentText(Main.getResources().getString("voice_sent_successfully"));
                alert.showAndWait();
            }
        });
        return result;
    }

    private void alertError(Throwable ex) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(Main.getResources().getString("error"));
        alert.setHeaderText(null);
        final String message;
        if ((ex instanceof CompletionException) && (ex.getCause() != null)) {
            message = ex.getCause().getLocalizedMessage();
        } else {
            message = ex.getLocalizedMessage();
        }
        alert.setContentText(message);
        alert.showAndWait();
    }
}
