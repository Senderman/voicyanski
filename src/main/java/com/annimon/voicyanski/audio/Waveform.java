package com.annimon.voicyanski.audio;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public final class Waveform {

    private final List<Rms> waveform;

    public Waveform() {
        waveform = new ArrayList<>();
    }

    public Waveform(List<Rms> waveform) {
        this.waveform = waveform;
    }

    public double getMaxTime() {
        final int size = size();
        if (size == 0) {
            return 0;
        }
        return waveform.get(size - 1).getPtsTime();
    }

    public void addRms(Rms rms) {
        waveform.add(rms);
    }

    public boolean isEmpty() {
        return waveform.isEmpty();
    }

    public int size() {
        return waveform.size();
    }

    public Stream<Rms> rms() {
        return waveform.stream();
    }
}
