package com.annimon.voicyanski.audio;

public final class Rms {

    private final int frame;
    private final double ptsTime;
    private final double level;

    public Rms(int frame, double ptsTime, double level) {
        this.frame = frame;
        this.ptsTime = ptsTime;
        this.level = level;
    }

    public int getFrame() {
        return frame;
    }

    public double getPtsTime() {
        return ptsTime;
    }

    public double getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Rms{" + "frame=" + frame + ", ptsTime=" + ptsTime + ", level=" + level + '}';
    }
}
