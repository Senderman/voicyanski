package com.annimon.voicyanski.controls;

import com.annimon.voicyanski.audio.Rms;
import com.annimon.voicyanski.audio.Waveform;
import java.util.DoubleSummaryStatistics;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public final class WaveformView extends Control {

    private final ObjectProperty<Waveform> waveformProperty = new SimpleObjectProperty<>();
    private final DoubleProperty startProperty = new SimpleDoubleProperty(0);
    private final DoubleProperty endProperty = new SimpleDoubleProperty(15);
    private final ObjectProperty<Duration> currentPlayTimeProperty = new SimpleObjectProperty<>();
    private ChangeListener<Duration> customCurrentTimeListener;

    public Waveform getWaveform() {
        return waveformProperty.get();
    }

    public void setCustomCurrentTimeListener(ChangeListener<Duration> customCurrentTimeListener) {
        this.customCurrentTimeListener = customCurrentTimeListener;
    }

    public void setWaveform(Waveform waveform) {
        waveformProperty.set(waveform);
    }

    public ObjectProperty<Waveform> waveformProperty() {
        return waveformProperty;
    }

    public double getStart() {
        return startProperty.get();
    }

    public void setStart(double start) {
        startProperty.set(start);
    }

    public DoubleProperty startProperty() {
        return startProperty;
    }

    public double getEnd() {
        return startProperty.get();
    }

    public void setEnd(double end) {
        endProperty.set(end);
    }

    public DoubleProperty endProperty() {
        return endProperty;
    }

    public Duration getCurrentPlayTime() {
        return currentPlayTimeProperty.get();
    }

    public void setCurrentPlayTime(Duration time) {
        currentPlayTimeProperty.set(time);
    }

    public ObjectProperty<Duration> currentPlayTimeProperty() {
        return currentPlayTimeProperty;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new WaveformViewSkin(this);
    }

    class WaveformViewSkin extends SkinBase<WaveformView> {

        private final Canvas canvas;

        @SuppressWarnings("unchecked")
        public WaveformViewSkin(WaveformView control) {
            super(control);
            canvas = new Canvas();
            canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, this::onMouseEvent);
            canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, this::onMouseEvent);
            ChangeListener listener = (observable, oldValue, newValue) -> {
                redraw(canvas.getWidth(), canvas.getHeight());
            };
            getChildren().add(canvas);
            startProperty.addListener(listener);
            endProperty.addListener(listener);
            currentPlayTimeProperty.addListener(listener);
        }

        private void onMouseEvent(MouseEvent event) {
            final Waveform wf = waveformProperty.getValue();
            if (wf == null) return;

            final double maxTime = wf.getMaxTime();
            final double seconds = event.getX() * maxTime / canvas.getWidth();
            Duration time = Duration.seconds(seconds);
            if (customCurrentTimeListener != null) {
                customCurrentTimeListener.changed(currentPlayTimeProperty, getCurrentPlayTime(), time);
            }
        }

        @Override
        protected void layoutChildren(double cx, double cy, double width, double height) {
            canvas.setWidth(width);
            canvas.setHeight(height);
            redraw(width, height);
        }

        private void redraw(double width, double height) {
            final Waveform wf = waveformProperty.getValue();
            final double halfHeight = height / 2;
            final GraphicsContext g = canvas.getGraphicsContext2D();
            g.setFill(Color.WHITE);
            g.fillRect(0, 0, width, height);
            g.setStroke(Color.LIGHTGREEN);
            g.setLineWidth(2);
            if (wf == null || wf.isEmpty()) {
                g.strokeLine(0, halfHeight, width, halfHeight);
            } else {
                final double maxTime = wf.getMaxTime();
                final double xStep = width / maxTime;
                final DoubleSummaryStatistics stat = wf.rms()
                        .mapToDouble(Rms::getLevel)
                        .summaryStatistics();
                final double yStep = halfHeight / (stat.getMax() - stat.getMin());
                wf.rms().forEach(rms -> {
                    double x = rms.getPtsTime() * xStep;
                    double level = (rms.getLevel() - stat.getMin()) * yStep;
                    g.strokeLine(x, halfHeight - level, x, halfHeight + level);
                });

                final double start = startProperty.get() * xStep;
                final double end = endProperty.get() * xStep;
                g.setFill(Color.grayRgb(0, 0.15));
                g.fillRect(0, 0, start, height);
                g.fillRect(end, 0, width - end, height);

                final Duration time = getCurrentPlayTime();
                if (time != null && !time.isUnknown()) {
                    final double timeX = time.toSeconds() * xStep;
                    g.setLineWidth(1);
                    g.setStroke(Color.RED);
                    g.strokeLine(timeX, 0, timeX, height);
                }
            }
        }
    }
}
