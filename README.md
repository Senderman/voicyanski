# Voicyanski

JavaFX application for easy cut and send audio to Telegram chat as voice messages.


## Requirements

 - JavaFX 8
 - `ffmpeg` with libopus encoder


## Building

Build jar with dependencies: `./gradlew shadowJar`. The executable jar will appear as `build/libs/Voicyanski-all.jar`

## Configuration

1. Open .jar file as zip archive and edit `app.properties`.
2. Set `telegram-bot-token` and `telegram-chat-id` properties
3. Also you can copy `app.properties` to `%home%/.config/voicyanski.conf`. This file will be read first. You don't need to fill in all props because missing props will fallback to `app.properties`.

## Running

Run `java -jar Voicyanski-all.jar` and drag&drop your music file on the app's window or run `java -jar Voicyanski-all.jar <music file>`. Supported formats: MP3, AAC, WAV
